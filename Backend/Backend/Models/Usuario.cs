﻿/*
 * Creación de clase modelo para la migración a la base de Datos.
 * Utilización de Guid como llave primaria.
 */
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Models
{
    public class Usuario
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [Column(TypeName = "varchar(50)")]
        public string User { get; set; }

        [Required]
        [Column(TypeName = "varchar(50)")]
        public string Pass { get; set; }

        [Required]
        [Column(TypeName = "varchar(20)")]
        public string Rol { get; set; }

        [Required]
        [Column(TypeName = "varchar(20)")]
        public string Permisos { get; set; }
    }
}
