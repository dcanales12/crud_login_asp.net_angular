import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'
import { Usuario } from '../models/usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  myApUrl = 'https://localhost:44330';
  myApiUrl = 'api/Usuario/';
  constructor(private http:HttpClient) { }

  guardarUsuario(usuario:Usuario): Observable<Usuario>{
    return this.http.post<Usuario>(this.myApUrl + this.myApiUrl,usuario);
  }
}
