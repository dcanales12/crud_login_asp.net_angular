import { Component, OnInit } from '@angular/core';
import {FormGroup,FormBuilder,Validators} from '@angular/forms';
import { Usuario } from 'src/app/models/usuario';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  form: FormGroup;
  constructor(private formBuilder: FormBuilder,private usuarioService: UsuarioService) {
    this.form = this.formBuilder.group({
      id:0,
      usuario:['',[Validators.required]],
      pass:['',[Validators.required]],
      rol:['',[Validators.required]],
      permiso:['',[Validators.required]],
    });
   }

  ngOnInit(): void {
  }

  guardarUsuario(){
    const usuario: Usuario ={
        
      }  
       
    }
    

}
